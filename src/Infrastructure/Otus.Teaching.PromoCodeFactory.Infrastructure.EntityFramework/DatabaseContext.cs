﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework
{
	/// <summary>
	/// Контекст
	/// </summary>
	public class DatabaseContext : DbContext
	{
		public DbSet<Employee> Employees { get; set; }

		public DbSet<Role> Roles { get; set; }
		public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
		{
		}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<Employee>()
				.HasMany(u => u.Roles)
				.WithOne(c => c.Employee)
				.IsRequired();
		}
    }
}
