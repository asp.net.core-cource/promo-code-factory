﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework
{
	public static class EntityFrameworkInstaller
	{
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<DatabaseContext>(optionsBuilder
                => optionsBuilder.UseNpgsql(connectionString));
            return services;
        }
    }
}
