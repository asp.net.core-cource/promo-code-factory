﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations
{
	public class RolesRepository : Repository<Role>, IRoleRepository
	{
		public RolesRepository(DatabaseContext context)	: base(context)
		{
		}
	}
}
