﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations
{
	public abstract class Repository<T>
		: IRepository<T>
		where T : BaseEntity
	{
		protected readonly DbContext Context;
		private readonly DbSet<T> _entitySet;
		protected Repository(DbContext context)
		{
			Context = context;
			_entitySet = Context.Set<T>();
		}

		public async Task AddEntityAsync(T entity)
		{
			await _entitySet.AddAsync(entity);
			SaveChanges();
		}

		public void DeleteEntity(T entity)
		{
			_entitySet.Remove(entity);
			SaveChanges();
		}

		public virtual IQueryable<T> GetAll(bool asNoTracking = false)
		{
			return asNoTracking ? _entitySet.AsNoTracking() : _entitySet;
		}

		public async Task<IEnumerable<T>> GetAllAsync()
		{
			return await GetAll().ToListAsync();
		}

		public async Task<T> GetByIdAsync(Guid id)
		{
			return await _entitySet.FindAsync((object)id);
		}

		public void UpdateEntityAsync(T entity)
		{
			Context.Entry(entity).State = EntityState.Modified;
			SaveChanges();
		}

		public virtual void SaveChanges()
		{
			Context.SaveChanges();
		}
	}
}
