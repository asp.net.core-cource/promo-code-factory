﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;

namespace Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations
{
	public class EmployeesRepository : Repository<Employee>, IEmployeeRepository
	{
		public EmployeesRepository(DatabaseContext context)	: base(context)
		{
		}
	}
}
