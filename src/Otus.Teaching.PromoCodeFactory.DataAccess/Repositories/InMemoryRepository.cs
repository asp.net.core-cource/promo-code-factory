﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public abstract class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

		public Task AddEntityAsync(T entity)
		{
            return Task.FromResult(Data = Data.Append(entity).ToList());
        }

		public void DeleteEntity(T entity)
		{
            Data = Data.Where(e => e.Id != entity.Id).ToList();
        }

        public abstract void UpdateEntityAsync(T entity);

		public void SaveChanges()
		{
			throw new NotImplementedException();
		}
	}
}