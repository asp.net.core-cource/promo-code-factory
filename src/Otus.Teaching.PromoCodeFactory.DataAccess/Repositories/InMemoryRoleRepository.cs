﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class InMemoryRoleRepository : InMemoryRepository<Role>, IRoleRepository
	{
		public InMemoryRoleRepository(IEnumerable<Role> data) : base(data)
		{
		}

		public override void UpdateEntityAsync(Role entity)
		{
			throw new NotImplementedException();
		}
	}
}
