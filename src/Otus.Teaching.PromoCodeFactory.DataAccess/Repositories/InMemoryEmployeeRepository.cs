﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class InMemoryEmployeeRepository : InMemoryRepository<Employee>, IEmployeeRepository
	{
		public InMemoryEmployeeRepository(IEnumerable<Employee> data) : base(data)
		{
		}

		public override void UpdateEntityAsync(Employee entity)
		{
			var updatedData = Data.First(e => e.Id == entity.Id) as Employee;

			if(updatedData == null)
				throw new Exception();

			updatedData.FirstName = entity.FirstName;
			updatedData.LastName = entity.LastName;
			updatedData.Email = entity.Email;

			entity = updatedData;
		}
	}
}
