﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Infrastructure.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Infrastructure.Repositories.Implementations;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    /// <summary>
    /// Регистратор сервиса
    /// </summary>
    public static class Registrar
	{
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings);
            return services.AddSingleton((IConfigurationRoot)configuration)
                .ConfigureContext(applicationSettings.ConnectionString)
                .InstallRepositories();
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
		{
            serviceCollection
            .AddSingleton(typeof(IEmployeeRepository), typeof(EmployeesRepository))
            .AddSingleton(typeof(IRoleRepository), typeof(RolesRepository));

            return serviceCollection;
        }
    }
}
