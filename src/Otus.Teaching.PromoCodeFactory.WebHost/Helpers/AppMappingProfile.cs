﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
	public class AppMappingProfile : Profile
	{
		public AppMappingProfile()
		{
			CreateMap<CreateEmployeeRequest, Employee>();
			CreateMap<UpdateEmployeeRequest, Employee>();
		}
	}
}
